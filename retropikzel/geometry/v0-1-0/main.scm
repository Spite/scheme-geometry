;> # Geometry
;> Library for 2D geometry utilities
(define-library
  (retropikzel geometry v0-1-0 main)
  (import (scheme base))
  (export geometry-point-make
          geometry-point?
          geometry-point-x-get
          geometry-point-x-set!
          geometry-point-y-get
          geometry-point-y-set!
          geometry-point-inside-rectangle?
          geometry-line-make
          geometry-line?
          geometry-line-a-get
          geometry-line-a-set!
          geometry-line-b-get
          geometry-line-b-set!
          geometry-rectangle-make
          geometry-rectangle?
          geometry-rectangle-a-get
          geometry-rectangle-a-set!
          geometry-rectangle-b-get
          geometry-rectangle-b-set!
          geometry-rectangle-c-get
          geometry-rectangle-c-set!
          geometry-rectangle-d-get
          geometry-rectangle-d-set!
          geometry-rectangle-position-get
          geometry-rectangle-position-set!
          geometry-rectangle-x-get
          geometry-rectangle-y-get
          geometry-rectangle-width-get
          geometry-rectangle-height-get)
  (begin

    ;> ## Records

    ;> ### geometry-point
    ;> Fields:
    ;> - x (number) X position of the point
    ;> - y (number) Y position of the point
    (define-record-type geometry-point
      (geometry-point-make x y)
      geometry-point?
      (x geometry-point-x-get geometry-point-x-set!)
      (y geometry-point-y-get geometry-point-y-set!))


    ;> ### geometry-rectangle
    ;> Fields:
    ;> - a (geometry-point) Top left corner when rectangle is made
    ;> - b (geometry-point) Top right corner when rectangle is made
    ;> - c (geometry-point) Bottom left corner when rectangle is made
    ;> - d (geometry-point) Bottom right corner when rectangle is made
    (define-record-type geometry-rectangle
      (geometry-rectangle-make a b c d)
      geometry-rectangle?
      (a geometry-rectangle-a-get geometry-rectangle-a-set!)
      (b geometry-rectangle-b-get geometry-rectangle-b-set!)
      (c geometry-rectangle-c-get geometry-rectangle-c-set!)
      (d geometry-rectangle-d-get geometry-rectangle-d-set!))

    ;> ### geometry-line
    ;> Fields:
    ;> - a (geometry-point) Start point of the line
    ;> - b (geometry-point) End point of the line
    (define-record-type geometry-line
      (geometry-line-make a b)
      geometry-line?
      (a geometry-line-a-get geometry-line-a-set!)
      (b geometry-line-b-get geometry-line-b-set!))

    ;> ## Procedures

    ;> ### geometry-point-make
    ;>
    ;> Arguments:
    ;> - x (number) X position of the point
    ;> - y (number) Y position of the point
    ;>
    ;> Returns:
    ;> - (geometry-point)

    ;> ### geometry-point?
    ;>
    ;> Arguments:
    ;> - object (object) Any object
    ;>
    ;> Returns:
    ;> - (boolean) Returns #t if object is geometry-point, otherwise #f

    ;> ### geometry-point-x-get
    ;>
    ;> Arguments:
    ;> - point (geometry-point) The point you want the x position of
    ;>
    ;> Returns:
    ;> - (number) The x position of the point

    ;> ### geometry-point-y-get
    ;>
    ;> Arguments:
    ;> - point (geometry-point) The point you want the y position of
    ;>
    ;> Returns:
    ;> - (number) The y position of the point

    ;> ### geometry-line-make
    ;>
    ;> Arguments:
    ;> - a (geometry-point) Start point of the line
    ;> - b (geometry-point) End point of the line
    ;>
    ;> Returns

    ;> ### geometry-line?
    ;>
    ;> Arguments:
    ;> - object (object) Object you want to test if it is geometry-line
    ;>
    ;> Returns:
    ;> - (boolean) If given object is geometry-line then returns #t, otherwise #f

    ;> ### geometry-line-a-get
    ;>
    ;> Arguments:
    ;> - line (geometry-line) Line you want to get the point a from
    ;>
    ;> Returns:
    ;> - (geometry-point) A point of given line

    ;> ### geometry-line-a-set!
    ;>
    ;> Arguments:
    ;> - line (geometry-line) Line you want to set the point a of
    ;> - point (geometry-point) New point that replaces the old point a

    ;> ### geometry-line-b-get
    ;>
    ;> Arguments:
    ;> - line (geometry-line) Line you want to get the point b from
    ;>
    ;> Returns:
    ;> - (geometry-point) B point of given line

    ;> ### geometry-line-b-set!
    ;>
    ;> Arguments:
    ;> - line (geometry-line) Line you want to set the point b of
    ;> - point (geometry-point) New point that replaces the old point b

    ;> ### geometry-rectangle-make
    ;>
    ;> Arguments:
    ;> - a (geometry-point) Top left corner of rectangle
    ;> - b (geometry-point) Top right corner of rectangle
    ;> - c (geometry-point) Bottom left corner of rectangle
    ;> - d (geometry-point) Bottom right corner of rectangle
    ;> Returns:
    ;> - (geometry-rectangle)

    ;> ### geometry-rectangle?
    ;>
    ;> Arguments:
    ;> - object (object) Any object
    ;> Returns:
    ;> - (boolean) Returns #t if object is geometry-rectangle, otherwise #f

    ;> ### geometry-rectangle-a-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) Rectangle you want the corner of
    ;>
    ;> Returns:
    ;> - (geometry-point) The a point of rectangle

    ;> ### geometry-rectangle-b-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) Rectangle you want the corner of
    ;>
    ;> Returns:
    ;> - (geometry-point) The b point of rectangle

    ;> ### geometry-rectangle-c-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) Rectangle you want the corner of
    ;>
    ;> Returns:
    ;> - (geometry-point) The c point of rectangle

    ;> ### geometry-rectangle-d-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) Rectangle you want the corner of
    ;>
    ;> Returns:
    ;> - (geometry-point) The d point of rectangle

    ;> ### geometry-point-inside-rectangle
    ;>
    ;> Arguments:
    ;> - point (geometry-point) The point you want to check if it is inside rectangle
    ;> - rectangle (geometry-rectangle) The rectangle you want to check if the point is in
    ;>
    ;> Returns:
    ;> - (boolean) If given point is inside given rectangle returns #t, othwerwise #f
    (define geometry-point-inside-rectangle?
      (lambda (point rectangle)
        (and (> (geometry-point-x-get point)
                (geometry-point-x-get (geometry-rectangle-a-get rectangle)))
             (< (geometry-point-x-get point)
                (geometry-point-x-get (geometry-rectangle-b-get rectangle)))
             (> (geometry-point-y-get point)
                (geometry-point-y-get (geometry-rectangle-a-get rectangle)))
             (< (geometry-point-y-get point)
                (geometry-point-y-get (geometry-rectangle-d-get rectangle))))))

    ;> ### geometry-rectangle-position-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) The rectangle you want position of
    ;>
    ;> Returns:
    ;> - (geometry-point) Left top corner of rectangle
    ; FIX If rectangle is trasnlated then point a might not be the top left corner anymore
    (define geometry-rectangle-position-get
      (lambda (rectangle)
        (geometry-rectangle-a-get rectangle)))

    ;> ### geometry-rectangle-x-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) The rectangle you want x position of
    ;>
    ;> Returns:
    ;> - (number) X position of the rectangles top left corner
    (define geometry-rectangle-x-get
      (lambda (rectangle)
        (geometry-point-x-get (geometry-rectangle-a-get rectangle))))

    ;> ### geometry-rectangle-y-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) The rectangle you want y position of
    ;>
    ;> Returns:
    ;> - (number) Y position of the rectangles top left corner
    (define geometry-rectangle-y-get
      (lambda (rectangle)
        (geometry-point-y-get (geometry-rectangle-a-get rectangle))))

    ;> ### geometry-rectangle-width-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) The rectangle you want width of
    ;>
    ;> Returns:
    ;> - (number) Width of the rectangle
    (define geometry-rectangle-width-get
      (lambda (rectangle)
        (- (geometry-point-x-get (geometry-rectangle-c-get rectangle))
           (geometry-point-x-get (geometry-rectangle-a-get rectangle)))))

    ;> ### geometry-rectangle-height-get
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) The rectangle you want height of
    ;>
    ;> Returns:
    ;> - (number) Height of the rectangle
    (define geometry-rectangle-height-get
      (lambda (rectangle)
        (- (geometry-point-y-get (geometry-rectangle-d-get rectangle))
           (geometry-point-y-get (geometry-rectangle-a-get rectangle)))))

    ; ## Macros

    ;> ### geometry-rectangle-position-set!
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle) The rectanle which position you want to set
    ;> - x (number) New x position for the top left corner
    ;> - y (number) New y position for the top left corner
    (define-syntax geometry-rectangle-position-set!
      (syntax-rules ()
        ((_ rectangle x y)
         (let ((original-width (geometry-rectangle-width-get rectangle))
               (original-height (geometry-rectangle-height-get rectangle)))
           (geometry-point-x-set! (geometry-rectangle-a-get rectangle)
                                  x)
           (geometry-point-y-set! (geometry-rectangle-a-get rectangle)
                                  y)
           (geometry-point-x-set! (geometry-rectangle-b-get rectangle)
                                  (+ x original-width))
           (geometry-point-y-set! (geometry-rectangle-b-get rectangle)
                                  y)
           (geometry-point-x-set! (geometry-rectangle-c-get rectangle)
                                  (+ x original-width))
           (geometry-point-y-set! (geometry-rectangle-c-get rectangle)
                                  (+ y original-height))
           (geometry-point-x-set! (geometry-rectangle-d-get rectangle)
                                  x)
           (geometry-point-y-set! (geometry-rectangle-d-get rectangle)
                                  (+ y original-height))))))

    ))

