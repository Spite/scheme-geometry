Utility library for geometry related stuff for R7RS Schemes.

For documentation see
[Wiki](https://codeberg.org/Spite/scheme-geometry/wiki/Documentation)

For bugs you can use the
[Bugs](https://codeberg.org/Spite/scheme-time/projects/909://codeberg.org/Spite/scheme-geometry/projects/9100)
